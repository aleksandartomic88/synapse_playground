﻿using Parquet;
using Parquet.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.Threading.Tasks;

namespace SynapseUtils
{
    class Program
    {
        public class Options
        {
            [Value(0, MetaName = "scenario_name", HelpText = "scenario name.", Required = true)]
            public string ScenarioName { get; set; }

            [Value(1, MetaName = "saconnection", HelpText = "Storage account connection string.", Required = true)]
            public string StorageAccountConnectionString { get; set; }
        }

        static async Task UploadToStorageAccountTest(string connectionString)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
            string defaultContainerName = "container1";

            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(defaultContainerName);
            await foreach (BlobItem blobItem in containerClient.GetBlobsAsync())
            {
                Console.WriteLine("\t" + blobItem.Name);
            }
        }

        static void ProduceParquet()
        {
            var idColumn = new DataColumn(
                new DataField<int?>("id"),
                new int[] { 1, 2 });

            var cityColumn = new DataColumn(
               new DataField<string>("city"),
               new string[] { "London", "Derby" });

            var field = new DataField<IEnumerable<int>>("population");
            var populationColumn = new DataColumn(
               field,
               new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
               new int[] { 0, 1, 1, 0, 1, 0, 1, 1, 1 });

            // create file schema
            var schema = new Schema(idColumn.Field, cityColumn.Field, populationColumn.Field);

            using (Stream fileStream = System.IO.File.OpenWrite(@"C:\Users\Aleksandar\temp\test_repetable_nested.parquet"))
            {
                using (var parquetWriter = new ParquetWriter(schema, fileStream))
                {
                    // create a new row group in the file
                    using (ParquetRowGroupWriter groupWriter = parquetWriter.CreateRowGroup())
                    {
                        groupWriter.WriteColumn(idColumn);
                        groupWriter.WriteColumn(cityColumn);
                        groupWriter.WriteColumn(populationColumn);
                    }
                }
            }
        }

        static void RunOptions(Options opts)
        {
            if (opts.ScenarioName == "ListBlobs")
            {
                Console.WriteLine("Using connection string: {0}", opts.StorageAccountConnectionString);
                UploadToStorageAccountTest(opts.StorageAccountConnectionString).Wait();
            }
            else if (opts.ScenarioName == "GenerateParquet")
            {
                ProduceParquet();
            }
            else
            {
                Console.WriteLine("Invalid scenario name");
            }
        }

        static void HandleParseError(IEnumerable<Error> errs)
        {
            errs.ToList().ForEach(Console.WriteLine);
            Environment.Exit(1);
        }

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(RunOptions)
                .WithNotParsed(HandleParseError);
        }
    }
}
