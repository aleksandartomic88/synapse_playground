﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WikiDataJsonExtractor
{
    public class ExtractorBase
    {
        private int[] TypeIds;

        protected WikiDataIndex index;

        private Config cfg;

        private IdColumnTypeMapper[] mappers;

        private IExtractorOutputer outputter;

        protected ExtractorBase(WikiDataIndex index, int[] typeIds, Config cfg, IdColumnTypeMapper[] mappers, IExtractorOutputer outputter)
        {
            this.TypeIds = typeIds;
            this.cfg = cfg;
            this.mappers = mappers;
            this.outputter = outputter;

            foreach (var mapper in mappers.Where(m => m.Type == typeof(PropertyValueMapper<List<NameIdPair>>)))
            {
                this.outputter.ValueListNames.Add(mapper.ColumnName);
            }

            foreach (var mapper in mappers.Where(m => m.Type == typeof(PropertyValueMapper<NameIdPair>)))
            {
                this.outputter.NameIdPairs.Add(mapper.ColumnName);
            }

            foreach (var mapper in mappers.Where(m => m.Type == typeof(PropertyValueMapper<double?>)))
            {
                this.outputter.DoubleMappers.Add(mapper.ColumnName);
            }

            foreach (var mapper in mappers.Where(m => m.Type == typeof(PropertyValueMapper<int?>)))
            {
                this.outputter.IntMappers.Add(mapper.ColumnName);
            }

            foreach (var mapper in mappers.Where(m => m.Type == typeof(PropertyValueMapper<DateTimeOffset?>)))
            {
                this.outputter.DateTimeMappers.Add(mapper.ColumnName);
            }

            this.index = index;
        }

        protected static JToken GetValue(JToken token, IEnumerable<string> path)
        {
            JToken curr = token;
            foreach (var p in path)
            {
                if (curr[p] != null)
                {
                    curr = curr[p];
                }
                else
                {
                    return null;
                }
            }

            return curr;
        }

        public void CreateItems()
        {
            List<BaseExtractorItem> items = new List<BaseExtractorItem>();
            int count = 0;
            const int batchCount = 1000000;
            int batchNum = 0;

            if (this.cfg.Profiler != null)
            {
                this.cfg.Profiler.StartMeasuring();
            }

            foreach (var indexEntry in index.Entries)
            {
                if (indexEntry.InstanceOf != null && indexEntry.InstanceOf.Intersect(TypeIds).Any())
                {
                    JObject rss = index.GetPageEntry(indexEntry);

                    BaseExtractorItem item = new BaseExtractorItem(this.mappers);
                    item.Id = indexEntry.Id;
                    item.Name = indexEntry.Name;

                    foreach (var valueListMapper in item.ValueListMappers)
                    {
                        valueListMapper.Value = new List<NameIdPair>();
                        FillProperyNameIdList(rss, valueListMapper);
                    }

                    foreach (var nameIdMapper in item.NameIdMappers)
                    {
                        FillPropertyNameIdPair(rss, nameIdMapper);
                    }

                    foreach (var mapper in item.DateTimeMappers)
                    {
                        FillDateTimeValue(rss, mapper);
                    }

                    foreach (var mapper in item.IntMappers)
                    {
                        FillValueMapper(rss, mapper);
                    }

                    foreach (var mapper in item.DoubleMappers)
                    {
                        FillValueMapper(rss, mapper);
                    }

                    items.Add(item);

                    if (items.Count == batchCount)
                    {
                        Console.WriteLine("Generating batch. Batch num {0}!", batchNum);
                        outputter.OutputItems(items, batchNum);
                        batchNum++;
                        items.Clear();
                    }
                }

                count++;
                if (cfg.Profiler != null && count % cfg.Profiler.ReportingSample == 0)
                {
                    cfg.Profiler.ReportProgress(count);
                }
            }

            outputter.OutputItems(items, batchNum);
        }

        protected void FillPropertyNameIdPair(JToken rss, PropertyValueMapper<NameIdPair> nameIdPair)
        {
            if (rss["claims"]["P" + nameIdPair.PropertyId] != null)
            {
                foreach (var node in rss["claims"]["P" + nameIdPair.PropertyId])
                {
                    JToken token = GetValue(node, new[] { "mainsnak", "datavalue", "value", "numeric-id" });
                    if (token != null)
                    {
                        int nodeId = (int)token;
                        var item = index.GetIndexEntry(IndexEntry.EntryType.Q, nodeId);

                        if (item != null)
                        {
                            nameIdPair.Value = new NameIdPair() { Id = nodeId, Name = item.Name };
                        }
                    }
                }
            }
        }

        protected void FillProperyNameIdList(JToken rss, PropertyValueMapper<List<NameIdPair>> nameIdPair)
        {
            if (rss["claims"]["P" + nameIdPair.PropertyId] != null)
            {
                foreach (var node in rss["claims"]["P" + nameIdPair.PropertyId])
                {
                    JToken token = GetValue(node, new[] { "mainsnak", "datavalue", "value", "numeric-id" });
                    if (token != null)
                    {
                        int nodeId = (int)token;
                        var item = index.GetIndexEntry(IndexEntry.EntryType.Q, nodeId);

                        if (item != null)
                        {
                            nameIdPair.Value.Add(new NameIdPair() { Id = nodeId, Name = item.Name });
                        }
                    }
                }
            }
        }

        protected static void FillDateTimeValue(JToken rss, PropertyValueMapper<DateTimeOffset?> mapper)
        {

            if (rss["claims"]["P" + mapper.PropertyId] != null)
            {
                foreach (var node in rss["claims"]["P" + mapper.PropertyId])
                {
                    JToken token = GetValue(node, new[] { "mainsnak", "datavalue", "value", "time" });
                    if (token != null)
                    {
                        string rawVal = (string)token;

                        try
                        {
                            string directParseRawVal = rawVal.Substring(1, rawVal.Length - 2);
                            DateTime val = DateTime.Parse(directParseRawVal);
                            mapper.Value = val;
                        }
                        catch (Exception)
                        {
                            int year = Int32.Parse(rawVal.Substring(0, 5));

                            if (year <= 1)
                            {
                                // Ok, this sucks, no datetime support for BC dates... 
                                // just push 1,1,1 for now...
                                continue;
                            }

                            mapper.Value = new DateTime(year, 1, 1);
                        }
                    }
                }
            }
        }

        protected private void FillValueMapper<T>(JToken rss, PropertyValueMapper<T> value)
        {
            if (rss["claims"]["P" + value.PropertyId] != null)
            {
                foreach (var node in rss["claims"]["P" + value.PropertyId])
                {
                    T val = node["mainsnak"]["datavalue"]["value"]["amount"].Value<T>();
                    value.Value = val;
                }
            }
        }
    }
}
