﻿using Newtonsoft.Json.Linq;
using Parquet;
using Parquet.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WikiDataJsonExtractor
{
    public class IndexEntry
    {
        public enum EntryType
        {
            P = 0,
            Q = 1,
        }

        public int Id { get; private set; }
        public EntryType Type { get; private set; }
        public string Name { get; private set; }
        public int[] InstanceOf { get; private set; }
        public UInt64 LineStart { get; set; }

        public IndexEntry(string rawEntry)
        {
            int startInstanceOf = rawEntry.LastIndexOf(",[");
            int endInstanceOf = rawEntry.LastIndexOf("],");

            string firstPart = rawEntry.Substring(0, startInstanceOf);
            string secondPart = rawEntry.Substring(endInstanceOf + 2, rawEntry.Length - endInstanceOf - 2);

            int indexofEndName = firstPart.IndexOf(',');

            string idString = firstPart.Substring(0, indexofEndName);
            Id = int.Parse(idString.Substring(1));
            if (idString[0] == 'P')
            {
                Type = EntryType.P;
            }
            else if (idString[0] == 'Q')
            {
                Type = EntryType.Q;
            }
            else
            {
                throw new InvalidDataException("Invalid data for Type");
            }

            Name = firstPart.Substring(indexofEndName + 1);
            if (startInstanceOf != endInstanceOf - 2)
            {
                string[] instanceOfraw = rawEntry.Substring(startInstanceOf + 2, endInstanceOf - startInstanceOf - 3).Split(",");
                InstanceOf = instanceOfraw.Select(r => Int32.Parse(r.Substring(1))).ToArray();
            }
            else
            {
                InstanceOf = new int[0];
            }

            LineStart = UInt64.Parse(secondPart);
        }

        public IndexEntry(int id, EntryType type, string name, int[] instanceOf, UInt64 lineStart)
        {
            this.Id = id;
            this.Name = name;
            this.Type = type;
            this.InstanceOf = instanceOf;
            this.LineStart = lineStart;
        }

        public override int GetHashCode()
        {
            int mask = (int)this.Type << 30;
            return this.Id | mask;
        }
    }

    public class WikiDataIndex
    {
        private Dictionary<int, IndexEntry> dictionary = new Dictionary<int, IndexEntry>();

        public Dictionary<int, IndexEntry>.ValueCollection Entries => dictionary.Values;

        private Config config;

        public WikiDataIndex(Config config)
        {
            this.config = config;

            if (config.IndexStorage == Config.IndexStorageType.Csv)
            {
                ReadCsvData();
            }
            else if (config.IndexStorage == Config.IndexStorageType.Parquet)
            {
                ReadParquetData();
            }
        }

        private Dictionary<string, ulong> ReadLinePositions()
        {
            var dictionary = new Dictionary<string, ulong>();
            using (var lineStartFile = File.OpenRead(config.LineStartFilePath))
            using (var lineStartFileStreamReader = new StreamReader(lineStartFile, Encoding.UTF8))
            {
                string line = null;

                while ((line = lineStartFileStreamReader.ReadLine()) != null)
                {
                    string[] splitted = line.Split("-");
                    dictionary.Add(splitted[0], UInt64.Parse(splitted[1]));
                }
            }

            return dictionary;
        }

        private void ReadParquetData()
        {
            int counter = 0;
            foreach (var file in Directory.GetFiles(config.ParquetIndexPath))
            {
                using (Stream fileStream = File.OpenRead(file))
                using (var parquetReader = new ParquetReader(fileStream))
                {
                    DataField[] dataFields = parquetReader.Schema.GetDataFields();

                    for (int i = 0; i < parquetReader.RowGroupCount; i++)
                    {
                        using (ParquetRowGroupReader groupReader = parquetReader.OpenRowGroupReader(i))
                        {
                            DataColumn[] columns = dataFields.Select(groupReader.ReadColumn).ToArray();

                            // get first column, for instance
                            DataColumn idColumn = columns[0];
                            DataColumn typeColumn = columns[1];
                            DataColumn nameColumn = columns[2];
                            DataColumn instanceOfColumn = columns[3];
                            DataColumn lineStartsColumn = columns[4];

                            // .Data member contains a typed array of column data you can cast to the type of the column
                            int[] ids = (int[])idColumn.Data;
                            bool[] isProperty = (bool[])typeColumn.Data;
                            string[] name = (string[])nameColumn.Data;
                            int?[] instanceOf = (int?[])instanceOfColumn.Data;
                            int[] instanceOfRepetitions = instanceOfColumn.RepetitionLevels;
                            int repetitionIndex = 0;
                            long[] lineStarts = (long[])lineStartsColumn.Data; ;

                            for (int ii = 0; ii < ids.Length; ii++)
                            {
                                List<int> instanceOfParsed = new List<int>();
                                if (instanceOf[repetitionIndex].HasValue)
                                {
                                    instanceOfParsed.Add(instanceOf[repetitionIndex].Value);
                                    repetitionIndex++;
                                    while (repetitionIndex < instanceOfRepetitions.Length && instanceOfRepetitions[repetitionIndex] == 1)
                                    {
                                        instanceOfParsed.Add(instanceOf[repetitionIndex].Value);
                                        repetitionIndex++;
                                    }
                                }
                                else
                                {
                                    repetitionIndex++;
                                }

                                var entry = new IndexEntry(
                                    ids[ii],
                                    isProperty[ii] ? IndexEntry.EntryType.P : IndexEntry.EntryType.Q,
                                    name[ii],
                                    instanceOfParsed.ToArray(),
                                    (ulong)lineStarts[ii]);
                                dictionary.Add(entry.GetHashCode(), entry);
                                counter++;

                                if (config.SampleBuildConfig != null)
                                {
                                    if (counter == config.SampleBuildConfig.ItemsToSample)
                                    {
                                        return;
                                    }
                                }
                            }

                            if (config.Profiler != null)
                            {
                                config.Profiler.ReportProgress(counter);
                            }
                        }
                    }
                }
            }
        }

        private void ReadCsvData()
        {
            int counter = 0;

            using (var indexFile = File.OpenRead(config.IndexFilePath))
            using (var lineStartFile = File.OpenRead(config.LineStartFilePath))
            using (var indexFileStreamReader = new StreamReader(indexFile, Encoding.UTF8))
            using (var lineStartFileStreamReader = new StreamReader(lineStartFile, Encoding.UTF8))
            {
                string indexLine = null;
                string lineStart = null;
                while ((indexLine = indexFileStreamReader.ReadLine()) != null &&
                    (lineStart = lineStartFileStreamReader.ReadLine()) != null)
                {
                    var entry = new IndexEntry(indexLine);
                    entry.LineStart = ulong.Parse(lineStart);
                    dictionary.Add(entry.GetHashCode(), entry);

                    counter++;
                    if (config.SampleBuildConfig != null)
                    {
                        if (counter == config.SampleBuildConfig.ItemsToSample)
                        {
                            break;
                        }
                    }

                    if (config.Profiler != null)
                    {
                        if (counter % config.Profiler.ReportingSample == 0)
                        {
                            config.Profiler.ReportProgress(counter);
                        }
                    }
                }
            }
        }

        public IndexEntry GetIndexEntry(IndexEntry.EntryType type, int id)
        {
            int mask = (int)type << 30;
            int dictionaryId = id | mask;

            if (config.TypeOfConfig == Config.ConfigType.Test)
            {
                // In test mode it is fine to miss the page since we don't load everything.
                //
                if (!dictionary.ContainsKey(dictionaryId))
                {
                    return Entries.First();
                }
            }

            if (!dictionary.ContainsKey(dictionaryId))
            {
                return null;
            }

            return dictionary[dictionaryId];
        }

        public JObject GetPageEntry(IndexEntry indexEntry)
        {
            using (var fileStream = File.OpenRead(config.MainDataPath))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                fileStream.Seek((long)indexEntry.LineStart, SeekOrigin.Begin);
                string line = streamReader.ReadLine();
                JObject rss = JObject.Parse(line.Substring(0, line.Length - 1));
                System.Diagnostics.Debug.Assert(rss["id"].ToString() == indexEntry.Type.ToString() + indexEntry.Id.ToString());
                return rss;
            }
        }

        public void OutputParquet()
        {
            this.config.Profiler.StartMeasuring();

            int totalEntries = this.Entries.Count;
            int batchSize = 10000000; // 10M
            int processed = 0;

            for (int batchCount = 0; batchCount * batchSize < totalEntries; batchCount++)
            {
                int entriesToTake = Math.Min(batchSize, totalEntries - processed);

                var entriesBatch = this.Entries
                    .Skip(processed)
                    .Take(entriesToTake).ToArray();

                processed += entriesToTake;

                var idColumn = new DataColumn(
                    new DataField<int>("id"),
                    entriesBatch.Select(entry => entry.Id).ToArray());

                var typeColumn = new DataColumn(
                    new DataField<bool>("is_property"),
                    entriesBatch.Select(entry => entry.Type == IndexEntry.EntryType.P).ToArray());

                var nameColumn = new DataColumn(
                    new DataField<string>("name"),
                    entriesBatch.Select(entry => entry.Name).ToArray());

                var lineStartColumn = new DataColumn(
                    new DataField<long>("line_start"),
                    entriesBatch.Select(entry => (long)entry.LineStart).ToArray());

                // Generate repetition level.
                List<int?> instanceOfIds = new List<int?>();
                List<int> repetitionLevelInstanceOf = new List<int>();

                foreach (var entry in entriesBatch)
                {
                    if (entry.InstanceOf == null || !entry.InstanceOf.Any())
                    {
                        instanceOfIds.Add(null);
                        repetitionLevelInstanceOf.Add(0);
                    }
                    else
                    {
                        bool first = true;
                        foreach (var instanceOf in entry.InstanceOf)
                        {
                            instanceOfIds.Add(instanceOf);

                            if (first)
                            {
                                repetitionLevelInstanceOf.Add(0);
                                first = false;
                            }
                            else
                            {
                                repetitionLevelInstanceOf.Add(1);
                            }
                        }
                    }
                }

                var instanceOfColumn = new DataColumn(
                    new DataField<IEnumerable<int?>>("instance_of"),
                    instanceOfIds.ToArray(),
                    repetitionLevelInstanceOf.ToArray());

                var schema = new Schema(idColumn.Field, typeColumn.Field, nameColumn.Field, instanceOfColumn.Field, lineStartColumn.Field);

                string fileName = string.Format(@"C:\Users\Aleksandar\temp\index_batch_{0}_v2.parquet", batchCount);
                using (Stream fileStream = System.IO.File.OpenWrite(fileName))
                {
                    using (var parquetWriter = new ParquetWriter(schema, fileStream))
                    {
                        // create a new row group in the file
                        using (ParquetRowGroupWriter groupWriter = parquetWriter.CreateRowGroup())
                        {
                            groupWriter.WriteColumn(idColumn);
                            groupWriter.WriteColumn(typeColumn);
                            groupWriter.WriteColumn(nameColumn);
                            groupWriter.WriteColumn(instanceOfColumn);
                            groupWriter.WriteColumn(lineStartColumn);
                        }
                    }
                }

                this.config.Profiler.ReportProgress(processed);
            }
        }
    }
}
