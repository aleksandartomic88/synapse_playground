﻿using System;

namespace WikiDataJsonExtractor
{
    public class IdColumnTypeMapper
    {
        public int Id;
        public string ColumnName;
        public Type Type;

        public IdColumnTypeMapper(int id, string columnName, Type type)
        {
            Id = id;
            ColumnName = columnName;
            Type = type;
        }
    }
}
