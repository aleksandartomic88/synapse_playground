﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WikiDataJsonExtractor
{
    public class CommandParser
    {
        private WikiDataIndex index;
        private Config cfg;
        const string Path = @"E:\wikidata\latest-all.json";

        public CommandParser(WikiDataIndex index, Config cfg)
        {
            this.index = index;
            this.cfg = cfg;
        }

        public void CommandLineLoop()
        {
            Console.WriteLine("Starting repl");
            while (true)
            {
                string line = Console.ReadLine();
                Console.WriteLine("-----------------");

                if (line == "exit")
                {
                    return;
                }
                else
                {
                    ParseCommand(line);
                }
            }
        }

        private void ParseCommand(string line)
        {
            const string GetAllCommand = "InstanceOfAndClaims";
            const string OutputParquet = "IndexToParquet";
            const string OutputCountriesParquet = "OutputCountriesParquet";
            const string OutputPeopleParquet = "OutputPeopleParquet";
            const string OutputCitiesParquet = "OutputCitiesParquet";

            if (line == GetAllCommand)
            {
                Console.WriteLine("Enter InstanceOf: ");
                Int32 instanceOf = Int32.Parse(Console.ReadLine());

                foreach (IndexEntry entry in index.Entries)
                {
                    if (entry.InstanceOf.Any(x => x == instanceOf))
                    {
                        Console.WriteLine($"EntryId {entry.Id}, EntryName {entry.Name}");
                        JObject rss = index.GetPageEntry(entry);

                        foreach (var claim in rss["claims"])
                        {
                            Console.WriteLine("Claim : {0}", claim.Path);
                            int claimId = Int32.Parse(claim.Path.Split(".")[1].Substring(1));
                            var propertyIndexEntry = index.GetIndexEntry(IndexEntry.EntryType.P, claimId);
                            Console.WriteLine("Property name: {0}", propertyIndexEntry.Name);
                        }
                    }
                }
            }
            else if (line == OutputParquet)
            {
                index.OutputParquet();
            }
            else if (line == OutputCountriesParquet)
            {
                IExtractorOutputer outputter = new ExtractorParquetOutputter("countries_{0}.parquet");
                var extractor = new CountryExtractor(index, cfg, outputter);
                extractor.CreateItems();
            }
            else if (line == OutputPeopleParquet)
            {
                IExtractorOutputer outputter = new ExtractorParquetOutputter("people_{0}.parquet");
                var extractor = new PeopleExtractor(index, cfg, outputter);
                extractor.CreateItems();
            }
            else if (line == OutputCitiesParquet)
            {
                IExtractorOutputer outputter = new ExtractorParquetOutputter("city_or_settlement_{0}.parquet");
                var extractor = new CityOrSettlementExtractor(index, cfg, outputter);
                extractor.CreateItems();
            }
            else
            {
                Console.WriteLine("Invalid command");
            }
        }
    }
}
