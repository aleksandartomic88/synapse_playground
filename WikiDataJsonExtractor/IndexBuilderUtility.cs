﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WikiDataJsonExtractor
{
    public static class IndexBuilderUtility
    {
        const string Path = @"E:\wikidata\latest-all.json";
        const string TempPath = @"E:\wikidata\temp\";

        public static string GetLineBeginningPositions(StopwatchProfiler profiler)
        {
            string fileBeginningsPositions = System.IO.Path.Combine(TempPath, "linestarts_v3.txt");
            List<Tuple<string, long>> positions = new List<Tuple<string, long>>();

            // First line.
            positions.Add(Tuple.Create("Q31", 2L));
            using (var fileStream = File.OpenRead(Path))
            using (var binaryReader = new BinaryReader(fileStream))
            {
                const long blockSize = 10000000;
                long numOfBlockNums = fileStream.Length / blockSize + 1;
                Console.WriteLine($"Number of blocks {numOfBlockNums}");

                for (long blockNum = 0; blockNum < numOfBlockNums; blockNum++)
                {
                    byte[] rawBytes = binaryReader.ReadBytes((int)blockSize);

                    for (long i = 1; i < rawBytes.Length - 1; i++)
                    {
                        if (rawBytes[i - 1] == 44 && rawBytes[i] == 10)
                        {
                            const int lookAheadSize = 100;
                            byte[] beginning = new byte[lookAheadSize];
                            if (lookAheadSize + i < rawBytes.Length)
                            {
                                for (int inner = 0; inner < lookAheadSize; inner++)
                                {
                                    beginning[inner] = rawBytes[i + 1 + inner];
                                }
                            }
                            else
                            {
                                // need to peek at next block and consolidate
                                // 
                                long oldPosition = binaryReader.BaseStream.Position;
                                binaryReader.BaseStream.Seek(blockNum * blockSize + i + 1, SeekOrigin.Begin);
                                beginning = binaryReader.ReadBytes(lookAheadSize);
                                binaryReader.BaseStream.Seek(oldPosition, SeekOrigin.Begin);
                            }

                            string firstWords = Encoding.UTF8.GetString(beginning, 0, beginning.Length);
                            int propertyBeginning =  firstWords.IndexOf("\"id\":\"") + 6;
                            int propertyEnd = firstWords.IndexOf("\",", propertyBeginning + 1);
                            string property = firstWords.Substring(propertyBeginning, propertyEnd - propertyBeginning);

                            positions.Add(Tuple.Create(property, blockNum * blockSize + i + 1));
                        }
                    }

                    if (blockNum % profiler.ReportingSample == 0)
                    {
                        profiler.ReportProgress((int)blockNum);
                    }
                }
            }

            using (var streamWriter = new StreamWriter(fileBeginningsPositions, append: false))
            {
                foreach (var position in positions)
                {
                    streamWriter.WriteLine($"{position.Item1}-{position.Item2}");
                }
            }

            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }

        static string PrintStatsForInstanceOfAndLinePos(StopwatchProfiler profiler)
        {
            const Int32 BufferSize = 4096;
            string fileBeginningsPositions = System.IO.Path.Combine(TempPath, "index_id_name_instanceof_linebeg.txt");
            string errorFile = System.IO.Path.Combine(TempPath, "errors.txt");
            int cnt = 0;
            int skipped = 0;

            using (var fileStream = File.OpenRead(Path))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            using (var streamWriter = new StreamWriter(fileBeginningsPositions, append: false))
            using (var errorWriter = new StreamWriter(errorFile, append: false))
            {
                streamReader.ReadLine();

                string line = null;
                while ((line = streamReader.ReadLine()) != null)
                {
                    try
                    {
                        JObject rss = JObject.Parse(line.Remove(line.Length - 1));

                        StringBuilder sb = new StringBuilder();
                        sb.Append(rss["id"]);
                        sb.Append(",");

                        // label (if no label use description)
                        JToken label = rss["labels"]["en"];
                        if (label != null)
                        {
                            sb.Append(label["value"]);
                        }
                        else
                        {
                            JToken description = rss["descriptions"]["en"];
                            if (description != null)
                            {
                                sb.Append(description["value"]);
                            }
                            else
                            {
                                skipped++;

                                if (skipped % 1000 == 0)
                                {
                                    Console.WriteLine($"Skipped {skipped}");
                                }
                            }
                        }

                        sb.Append(",");

                        // print instance of fields.
                        //
                        if (rss["claims"] != null)
                        {
                            sb.Append("[");

                            if (rss["claims"]["P31"] != null)
                            {

                                foreach (var instanceof in rss["claims"]["P31"])
                                {
                                    sb.Append(instanceof["mainsnak"]["datavalue"]["value"]["id"]);
                                    sb.Append(",");
                                }
                            }

                            sb.Append("],");
                        }

                        sb.Append(fileStream.Position);
                        streamWriter.WriteLine(sb.ToString());

                        cnt++;

                        if (cnt % 10000 == 0)
                        {
                            Console.WriteLine($"Processed {cnt}.");
                        }
                    }
                    catch (Exception ex)
                    {
                        errorWriter.WriteLine(line);
                        errorWriter.WriteLine(ex);
                        errorWriter.WriteLine("==================");
                    }
                }
            }

            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }
    }
}
