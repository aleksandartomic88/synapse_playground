﻿using System;
using System.Collections.Generic;

namespace WikiDataJsonExtractor
{
    public class CountryExtractor : ExtractorBase
    {
        static IdColumnTypeMapper[] mappers =
            new IdColumnTypeMapper[]
            {
                new IdColumnTypeMapper(31, "Instance_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(361, "Part_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(122, "Goverment_Types", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(463, "Member_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(138, "Named_After", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(37, "Official_Language", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(85, "Anthem", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(30, "Continent", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(36, "Capital", typeof(PropertyValueMapper<NameIdPair>)),

                new IdColumnTypeMapper(2250, "Life_Expectancy", typeof(PropertyValueMapper<double?>)),
                new IdColumnTypeMapper(571, "Inception", typeof(PropertyValueMapper<DateTimeOffset?>)),
                new IdColumnTypeMapper(1082, "Population", typeof(PropertyValueMapper<int?>)),
            };

        const int CountryTypeId = 6256;

        public CountryExtractor(WikiDataIndex index, Config cfg, IExtractorOutputer outputter) : base(index, new[] { CountryTypeId }, cfg, mappers, outputter) { }
    }
}
