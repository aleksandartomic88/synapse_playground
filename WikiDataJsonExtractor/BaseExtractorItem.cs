﻿using System;
using System.Collections.Generic;

namespace WikiDataJsonExtractor
{
    public class BaseExtractorItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<PropertyValueMapper<List<NameIdPair>>> ValueListMappers = new List<PropertyValueMapper<List<NameIdPair>>>();

        public List<PropertyValueMapper<NameIdPair>> NameIdMappers = new List<PropertyValueMapper<NameIdPair>>();

        public List<PropertyValueMapper<double?>> DoubleMappers = new List<PropertyValueMapper<double?>>();

        public List<PropertyValueMapper<int?>> IntMappers = new List<PropertyValueMapper<int?>>();

        public List<PropertyValueMapper<DateTimeOffset?>> DateTimeMappers = new List<PropertyValueMapper<DateTimeOffset?>>();

        public BaseExtractorItem(IEnumerable<IdColumnTypeMapper> mappers)
        {
            foreach (var mapper in mappers)
            {
                if (mapper.Type == typeof(PropertyValueMapper<List<NameIdPair>>))
                {
                    ValueListMappers.Add(new PropertyValueMapper<List<NameIdPair>>(mapper.Id));
                }
                else if (mapper.Type == typeof(PropertyValueMapper<NameIdPair>))
                {
                    NameIdMappers.Add(new PropertyValueMapper<NameIdPair>(mapper.Id));
                }
                else if (mapper.Type == typeof (PropertyValueMapper<double?>))
                {
                    DoubleMappers.Add(new PropertyValueMapper<double?>(mapper.Id));
                }
                else if (mapper.Type == typeof (PropertyValueMapper<int?>))
                {
                    IntMappers.Add(new PropertyValueMapper<int?>(mapper.Id));
                }
                else if (mapper.Type == typeof (PropertyValueMapper<DateTimeOffset?>))
                {
                    DateTimeMappers.Add(new PropertyValueMapper<DateTimeOffset?>(mapper.Id));
                }
                else
                {
                    throw new InvalidCastException("Unknown type");
                }
            }
        }
    }
}
