﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WikiDataJsonExtractor
{
    public class Config
    {
        public class SampleBuild
        {
            public uint ItemsToSample { get; set; }
        }

        public enum IndexStorageType
        { 
            Csv,
            Parquet,
        }

        public enum ConfigType
        {
            Full,
            Test,
        }

        public SampleBuild SampleBuildConfig { get; private set; }

        public StopwatchProfiler Profiler { get; private set; }

        public string IndexFilePath { get; private set; }

        public string LineStartFilePath { get; private set; }

        public string MainDataPath { get; private set; }

        public string ParquetIndexPath { get; private set; }

        public ConfigType TypeOfConfig { get; private set; }

        public IndexStorageType IndexStorage { get; private set; }

        const string TempPath = @"E:\wikidata\temp\";

        public static Config GetDefaultConfig(StopwatchProfiler profiler)
        {
            Config cfg = new Config();
            cfg.Profiler = profiler;
            cfg.LineStartFilePath = System.IO.Path.Combine(TempPath, "linestarts_v3.txt");
            cfg.MainDataPath = @"E:\wikidata\latest-all.json";
            cfg.IndexFilePath = System.IO.Path.Combine(TempPath, "index_id_name_instanceof_linebeg.txt");
            cfg.ParquetIndexPath = @"C:\Users\Aleksandar\wiki_index_build";
            cfg.IndexStorage = IndexStorageType.Parquet;
            cfg.TypeOfConfig = ConfigType.Full;
            return cfg;
        }

        public static Config GetDefaultTestConfig(StopwatchProfiler profiler = null)
        {
            Config cfg = new Config();
            cfg.Profiler = profiler;
            cfg.SampleBuildConfig = new Config.SampleBuild();
            cfg.SampleBuildConfig.ItemsToSample = 1000;
            cfg.LineStartFilePath = System.IO.Path.Combine(TempPath, "linestarts_v3.txt");
            cfg.MainDataPath = @"E:\wikidata\latest-all.json";
            cfg.IndexFilePath = System.IO.Path.Combine(TempPath, "index_id_name_instanceof_linebeg.txt");
            cfg.ParquetIndexPath = @"C:\Users\Aleksandar\wiki_index_build";
            cfg.IndexStorage = IndexStorageType.Parquet;
            cfg.TypeOfConfig = ConfigType.Test;
            return cfg;
        }
    }
}
