﻿using System;
using System.Collections.Generic;

namespace WikiDataJsonExtractor
{
    public class NameIdPair
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public static int? GetId(PropertyValueMapper<NameIdPair> mapper)
        {
            if (mapper.Value != null)
            {
                return mapper.Value.Id;
            }
            else
            {
                return null;
            }
        }

        public static string GetName(PropertyValueMapper<NameIdPair> mapper)
        {
            if (mapper.Value != null)
            {
                return mapper.Value.Name;
            }
            else
            {
                return null;
            }
        }
    }

    public class PropertyValueMapper<T>
    {
        public int PropertyId { get; set; }
        public T Value { get; set; }
        public PropertyValueMapper(int propertyId)
        {
            this.PropertyId = propertyId;
        }
    }
}
