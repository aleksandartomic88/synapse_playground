﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WikiDataJsonExtractor
{
    public class PeopleExtractor : ExtractorBase
    {
        static IdColumnTypeMapper[] mappers =
            new IdColumnTypeMapper[]
            {
                new IdColumnTypeMapper(31, "Instance_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(361, "Part_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(463, "Member_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(1344, "participant of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(1830, "owner_of", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(21, "sex_or_gender", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(569, "date_of_birth", typeof(PropertyValueMapper<DateTimeOffset?>)),
                new IdColumnTypeMapper(570, "date_of_death", typeof(PropertyValueMapper<DateTimeOffset?>)),
                new IdColumnTypeMapper(19, "place_of_birth", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(20, "place_of_death", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(1196, "manner_of_death", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(509, "cause_of_death", typeof(PropertyValueMapper<NameIdPair>)),

                new IdColumnTypeMapper(27, "country of citizenship", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(22, "father", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(25, "mother", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(3373, "siblings", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(26, "spouse", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(40, "children", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(451, "unmarried_partners", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(53, "family", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(172, "ethnic_group", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(103, "native_language", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(1412, "spoken_languages", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(140, "religion", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(106, "occupation", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(101, "field_of_work", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(97, "noble_title", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(39, "position_held", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(108, "employers", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(737, "influenced_by", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(737, "awards_received", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(1411, "nominated_for", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(69, "educated_at", typeof(PropertyValueMapper<List<NameIdPair>>)),

                new IdColumnTypeMapper(551, "residence", typeof(PropertyValueMapper<List<NameIdPair>>)),
            };

        public PeopleExtractor(WikiDataIndex index, Config cfg, IExtractorOutputer outputter) : base(index, new[] { 5 }, cfg, mappers, outputter) { }
    }
}
