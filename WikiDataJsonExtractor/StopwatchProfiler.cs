﻿using System;
using System.Diagnostics;

namespace WikiDataJsonExtractor
{
    public class StopwatchProfiler
    {
        private class ProcessedSnapshot
        {
            public TimeSpan Elapsed { get; private set; }
            public int Processed { get; private set; }

            public ProcessedSnapshot(TimeSpan elapsed, int processed)
            {
                this.Elapsed = elapsed;
                this.Processed = processed;
            }

            public ProcessedSnapshot() : this(TimeSpan.Zero, 0)
            {
            }
        }

        private Stopwatch stopwatch = new Stopwatch();
        private ProcessedSnapshot processedLastSnapshot = new ProcessedSnapshot();

        public UInt32 ReportingSample { get; private set; }

        private StopwatchProfiler()
        { }

        public static void Profile(Func<StopwatchProfiler, string> codeBlock, UInt32 reportingSample = 10000)
        {
            StopwatchProfiler profiler = new StopwatchProfiler();
            profiler.ReportingSample = reportingSample;

            profiler.stopwatch.Start();
            string description = codeBlock(profiler);
            profiler.stopwatch.Stop();
            TimeSpan ts = profiler.stopwatch.Elapsed;

            Console.WriteLine("Elapsed {0} for {1}", ts.TotalSeconds, description);
        }

        public void ReportProgress(int processedTotal)
        {
            TimeSpan elapsedTotal = stopwatch.Elapsed;

            double diff = processedTotal - processedLastSnapshot.Processed;
            TimeSpan elapsedDiff = elapsedTotal - processedLastSnapshot.Elapsed;

            double itemsPerSecond = diff / elapsedDiff.TotalSeconds;
            double memoryGbs = (GC.GetTotalMemory(false) / (1024.0 * 1024.0 * 1024.0));
            Console.WriteLine($"Processed {processedTotal}, speed {itemsPerSecond} item/sec, total elapsed {elapsedDiff.TotalSeconds}s, memory {memoryGbs}gbs.");
        }

        public void StartMeasuring()
        {
            stopwatch.Restart();
            this.processedLastSnapshot = new ProcessedSnapshot();
        }
    }
}
