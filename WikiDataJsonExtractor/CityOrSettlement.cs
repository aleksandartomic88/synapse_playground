﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WikiDataJsonExtractor
{
    public class CityOrSettlementExtractor : ExtractorBase
    {
        static IdColumnTypeMapper[] mappers =
            new IdColumnTypeMapper[]
            {
                new IdColumnTypeMapper(31, "Instance_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(361, "Part_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(17, "Country", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(1376, "Capital_Of", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(793, "Significant_events", typeof(PropertyValueMapper<List<NameIdPair>>)),
                new IdColumnTypeMapper(1830, "owner_of", typeof(PropertyValueMapper<List<NameIdPair>>)),

                // TODO: How to parse coordinates?
                // This will need some polishing...
                new IdColumnTypeMapper(625, "Coordinate_location", typeof(PropertyValueMapper<List<double?>>)),

                new IdColumnTypeMapper(155, "follows", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(156, "followed_by", typeof(PropertyValueMapper<NameIdPair>)),

                new IdColumnTypeMapper(156, "head_of_government", typeof(PropertyValueMapper<NameIdPair>)),
                new IdColumnTypeMapper(1082, "Population", typeof(PropertyValueMapper<int?>)),
                new IdColumnTypeMapper(281, "Postal_code", typeof(PropertyValueMapper<int?>)),
                new IdColumnTypeMapper(2046, "Area", typeof(PropertyValueMapper<double?>)),
                new IdColumnTypeMapper(571, "Inception", typeof(PropertyValueMapper<DateTimeOffset?>)),
                new IdColumnTypeMapper(3529, "median_income", typeof(PropertyValueMapper<double?>)),
            };

        private readonly int[] TypeIds = new int[]
        {
            // Human settlement
            486972,
            // City
            515,
        };

        public CityOrSettlementExtractor(WikiDataIndex index, Config cfg, IExtractorOutputer outputter) : base(index, new[] { 5 }, cfg, mappers, outputter) { }
    }
}
