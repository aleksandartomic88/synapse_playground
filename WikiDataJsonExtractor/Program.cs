﻿namespace WikiDataJsonExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            WikiDataIndex index = null;

            StopwatchProfiler.Profile((profiler) =>
            {
                Config config = Config.GetDefaultConfig(profiler);
                index = new WikiDataIndex(config);
                CommandParser commandParser = new CommandParser(index, config);
                commandParser.CommandLineLoop();
                return "Exit";
            }, reportingSample: 100000);
        }
    }
}
