﻿using Parquet;
using Parquet.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WikiDataJsonExtractor
{
    public interface IExtractorOutputer
    {
        public void OutputItems(IEnumerable<BaseExtractorItem> items, int batchNum);
        public List<string> ValueListNames { get; set; }
        public List<string> NameIdPairs { get; set; }
        public List<string> DateTimeMappers { get; set; }
        public List<string> IntMappers { get; set; }
        public List<string> DoubleMappers { get; set; }
    }

    public abstract class ExtractorOutputter : IExtractorOutputer
    {
        public List<string> ValueListNames { get; set; } = new List<string>();
        public List<string> NameIdPairs { get; set; } = new List<string>();

        public List<string> DateTimeMappers { get; set; } = new List<string>();

        public List<string> IntMappers { get; set; } = new List<string>();

        public List<string> DoubleMappers { get; set; } = new List<string>();

        public abstract void OutputItems(IEnumerable<BaseExtractorItem> items, int batchNum);
    }

    public class ExtractorParquetOutputter : ExtractorOutputter 
    {
        private string outputPathPattern;

        public ExtractorParquetOutputter(
            string outputPathPattern)
        {
            this.outputPathPattern = outputPathPattern;
        }

        private static Tuple<List<int>, List<T>> GenerateRepetitionColumn<T>(IEnumerable<IEnumerable<T>> data)
            where T : class
        {
            Tuple<List<int>, List<T>> result = Tuple.Create(new List<int>(), new List<T>());

            foreach (var item in data)
            {
                if (item == null || !item.Any())
                {
                    result.Item1.Add(0);
                    result.Item2.Add(null);
                }
                else
                {
                    bool first = true;
                    foreach (var innerItem in item)
                    {
                        result.Item2.Add(innerItem);

                        if (first)
                        {
                            result.Item1.Add(0);
                            first = false;
                        }
                        else
                        {
                            result.Item1.Add(1);
                        }
                    }
                }
            }

            System.Diagnostics.Debug.Assert(result.Item1.Count == result.Item2.Count);
            return result;
        }

        public override void OutputItems(IEnumerable<BaseExtractorItem> items, int batchNum)
        {
            List<DataColumn> columns = new List<DataColumn>();
            columns.Add(new DataColumn(
                new DataField<int>("id"),
                items.Select(entry => entry.Id).ToArray()));

            columns.Add(new DataColumn(
                new DataField<string>("name"),
                items.Select(entry => entry.Name).ToArray()));

            foreach (var index in NameIdPairs.Zip(Enumerable.Range(0, NameIdPairs.Count()), (a,b) => Tuple.Create(a,b)))
            {
                columns.Add(new DataColumn(
                    new DataField<string>(index.Item1),
                    items.Select(entry => NameIdPair.GetName(entry.NameIdMappers[index.Item2])).ToArray()));

                columns.Add(new DataColumn(
                    new DataField<int?>(index.Item1 + "_id"),
                    items.Select(entry => NameIdPair.GetId(entry.NameIdMappers[index.Item2])).ToArray()));
            }

            foreach (var index in this.ValueListNames.Zip(Enumerable.Range(0, ValueListNames.Count()), (a,b) => Tuple.Create(a,b)))
            {
                var repDataInstanceOf = GenerateRepetitionColumn(items.Select(c => c.ValueListMappers[index.Item2].Value));
                columns.Add(new DataColumn(
                    new DataField<IEnumerable<int?>>(index.Item1 + "_id"),
                    repDataInstanceOf.Item2.Select(i => i?.Id).ToArray(),
                    repDataInstanceOf.Item1.ToArray()));

                columns.Add(new DataColumn(
                    new DataField<IEnumerable<string>>(index.Item1),
                    repDataInstanceOf.Item2.Select(i => i?.Name).ToArray(),
                    repDataInstanceOf.Item1.ToArray()));

                System.Diagnostics.Debug.Assert(items.Count() == repDataInstanceOf.Item1.Where(x => x == 0).Count());
            }

            foreach (var index in this.DateTimeMappers.Zip(Enumerable.Range(0, this.DateTimeMappers.Count()), (a,b) => Tuple.Create(a,b)))
            {
                columns.Add(new DataColumn(
                    new DataField<DateTimeOffset?>(index.Item1),
                    items.Select(c => c.DateTimeMappers[index.Item2].Value).ToArray()));
            }

            foreach (var index in this.IntMappers.Zip(Enumerable.Range(0, this.IntMappers.Count()), (a,b) => Tuple.Create(a,b)))
            {
                columns.Add(new DataColumn(
                    new DataField<int?>(index.Item1),
                    items.Select(c => c.IntMappers[index.Item2].Value).ToArray()));
            }

            foreach (var index in this.DoubleMappers.Zip(Enumerable.Range(0, this.DoubleMappers.Count()), (a,b) => Tuple.Create(a,b)))
            {
                columns.Add(new DataColumn(
                    new DataField<double?>(index.Item1),
                    items.Select(c => c.DoubleMappers[index.Item2].Value).ToArray()));
            }

            var schema = new Schema(columns.Select(c => c.Field).ToArray());

            string outputPath = string.Format(this.outputPathPattern, batchNum);
            using (Stream fileStream = File.OpenWrite(outputPath))
            {
                using (var parquetWriter = new ParquetWriter(schema, fileStream))
                {
                    using (ParquetRowGroupWriter groupWriter = parquetWriter.CreateRowGroup())
                    {
                        foreach (var column in columns)
                        {
                            groupWriter.WriteColumn(column);
                        }
                    }
                }
            }
        }
    }
}
