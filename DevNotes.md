The most common values for all people that are instance of Human (Q5) are following:
* Top 20 instance of: human
* Top 20 instance of: author
* Top 20 instance of: twin (not sure what twean is)
* Top 20 instance of: notname
* Top 20 instance of: billionaire
* Top 20 instance of: professional name
* Top 20 instance of: Wikimedia duplicated page
* Top 20 instance of: bishop
* Top 20 instance of: pseudonym
* Top 20 instance of: human who may be fictional
* Top 20 instance of: veteran
* Top 20 instance of: anonymous master
* Top 20 instance of: Holocaust victim
* Top 20 instance of: centenarian
* Top 20 instance of: bog body
* Top 20 instance of: fictional character
* Top 20 instance of: Wikimedia disambiguation page
* Top 20 instance of: oldest human
* Top 20 instance of: legendary figure
* Top 20 instance of: journalist