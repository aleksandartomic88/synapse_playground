﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using WikiDataJsonExtractor;
using System.Linq;
using System;
using System.Collections.Generic;

namespace WikiDataTests
{
    public class TestOutputter : ExtractorOutputter
    {
        private Action<List<BaseExtractorItem>, IExtractorOutputer> assertions;

        public TestOutputter(Action<List<BaseExtractorItem>, IExtractorOutputer> assertions)
        {
            this.assertions = assertions;
        }

        public override void OutputItems(IEnumerable<BaseExtractorItem> items, int batchNum)
        {
            assertions(items.ToList(), this);
        }
    }

    [TestClass]
    public class ParsingTests
    {
        private static WikiDataIndex index;
        private static Config config;

        [ClassInitialize]
        public static void Setup(TestContext tc)
        {
            config = Config.GetDefaultTestConfig();
            index = new WikiDataIndex(config);
        }

        [TestMethod]
        public void ReadPageFromIndex()
        {
            foreach (var entry in index.Entries.Take(10))
            {
                JObject rss = index.GetPageEntry(entry);
                Assert.IsTrue(rss["id"].ToString() == entry.Type.ToString() + entry.Id.ToString());
            }
        }

        [TestMethod]
        public void ParseCountries()
        {
            TestOutputter to = new TestOutputter((l, outputter) =>
            {
                var belgium = l.First();
                Assert.IsTrue(belgium.DateTimeMappers[0].PropertyId == 571);
                Assert.IsTrue(belgium.DateTimeMappers[0].Value.Value.Year == 1830);
                Assert.IsTrue(belgium.Id == 31);
                Assert.IsTrue(belgium.Name == "Belgium");

                Assert.IsTrue(belgium.IntMappers[0].PropertyId == 1082);
                Assert.IsTrue(belgium.IntMappers[0].Value.Value == 11372068);

                Assert.IsTrue(belgium.NameIdMappers[0].PropertyId == 138);
                Assert.IsTrue(belgium.NameIdMappers[0].Value.Id == 206443);

                Assert.IsTrue(belgium.ValueListMappers[0].Value.Select(x => x.Id).SequenceEqual(new int[] { 3624078, 43702, 6256, 20181813 }));
            });

            (new CountryExtractor(index, config, to)).CreateItems();
        }

        [TestMethod]
        public void ParsePersons()
        {
            TestOutputter to = new TestOutputter((l, outputter) =>
            {
                var gw = l.First();
                Assert.IsTrue(gw.DateTimeMappers[0].PropertyId == 569);
                Assert.IsTrue(gw.DateTimeMappers[0].Value.Value.Year == 1732);
                Assert.IsTrue(gw.Id == 23);
                Assert.IsTrue(gw.Name == "George Washington");

                Assert.IsTrue(gw.NameIdMappers[0].PropertyId == 21);
                Assert.IsTrue(gw.NameIdMappers[0].Value.Id == 6581097);

                Assert.IsTrue(gw.ValueListMappers[0].Value.Select(x => x.Id).SequenceEqual(new int[] { 5 }));
            });

            (new PeopleExtractor(index, config, to)).CreateItems();
        }
    }
}
