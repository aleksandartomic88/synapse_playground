
-- ok this works
select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*index*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON (instance_of) as instance_of_id
where name = 'Belgium' and id = 31 and value = 6256

-- this is pretty slow, it took50s
select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
where name = 'Belgium'

-- trying to get all countries (35s)
select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON (instance_of) as instance_of_id
where value = 6256 -- 6256 is country list

-- 
select count(*) from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON (instance_of) as instance_of_id
where value = 8502 -- mountain range (392415 total)


-- number of items per category
select value as category, count(*) as total_cnt from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON (instance_of) as instance_of_id
group by value
order by total_cnt desc
-- Q13442814 is Journal article
-- 

-- join to get category names as well (this gets us to around 1min)
with count_per_instance_of(category, total_cnt)
as (
select value as category, count(*) as total_cnt from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON (instance_of) as instance_of_id
group by value
) select category, total_cnt, data.name
from count_per_instance_of
join openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
on count_per_instance_of.category = data.id
order by total_cnt desc

select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
where name LIKE '%Nikola%'
go

CREATE VIEW WikiMainIndex as
(
select * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as data
)
go

-- 100s
with all_da(id, name, category)
as
(
	select id, name, value as category from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*index*.parquet', format='parquet') as WikiMainIndex
	CROSS APPLY 
	OPENJSON (instance_of) as instance_of_id
	where name LIKE '%douglas adams%'
) select all_da.id, all_da.name, w.name
from all_da
join openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*.parquet', format='parquet') as w
on all_da.category = w.id
go



select * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/countries.parquet', format='parquet') as data
where name = 'Nepal'
order by life_expectancy desc

select * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/countries_v2.parquet', format='parquet') as data
where name = 'Serbia'
order by population desc

-- 37s...
select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*index*.parquet', format='parquet') as data
where name = 'Raghu Ramakrishnan'
go

select count(*) from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*index*.parquet', format='parquet') as data
-- rows 77897789


select top 1000 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
--where name = 'Raghu Ramakrishnan'

-- 6m rows
select count(*) from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data

select count(*) as cnt, sex_or_gender from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
group by sex_or_gender
order by cnt desc

-- seems like I wasn't able to parse 1/2 of them...
select count(*) cnt, DATEPART(year, date_of_birth) as date_of_birth from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
where date_of_birth is not null
group by DATEPART(year, date_of_birth) 
order by cnt desc


-- errors are trully broken...
select top 1000 name, id from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
where DATEPART(year, date_of_birth) > 2020

-- this is not ok
select top 10 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON ([country of citizenship])
where value = 'Japan'
--where name = 'Belgium' and id = 31 and value = 6256

select top 10 name, [country of citizenship] from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
where id = 9036

select top 10 * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
where id = 9036


-- ok, data is not correct...
select value as langauge, count(*) as cnt from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
CROSS APPLY
OPENJSON ([spoken_languages]) as coc
group by value
order by cnt desc

-- Request to perform an external distributed computation has failed with error "Query: {561D2FD5-69A5-408A-997A-88061C28BCFC} failed with: Workload task graph for query with id: [Stmt:{927A9BC1-5EE4-4FA9-B53D-9E19A7AE6D8F}]_[DQHash:0x3344618F95AE076A]_[Sch:b8323208-37f6-4b7a-b915-25d91f7405ff]_[Query:{561D2FD5-69A5-408A-997A-88061C28BCFC}] failed because one or more of its workload tasks has failed with failures[Workload Task:[Stmt:{927A9BC1-5EE4-4FA9-B53D-9E19A7AE6D8F}]_[DQHash:0x3344618F95AE076A]_[Sch:b8323208-37f6-4b7a-b915-25d91f7405ff]_[Query:{561D2FD5-69A5-408A-997A-88061C28BCFC}]_[WT:645]_[TG:645]_1_1 failed because one or more of execution tasks failed. Inner exceptions have details.[System.Data.SqlClient.SqlException (0x80131904): Column 'participant of' of type 'VARCHAR' is not compatible with external data type 'Parquet column is of nested type'.

select top 10 date_of_death from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data


select count(*) cnt, DATEPART(year, date_of_death) as dod from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON ([country of citizenship])
where value = 'Japan'
	and date_of_death is not null
	and DATEPART(year, date_of_death) < 2000
group by DATEPART(year, date_of_death) 
order by cnt desc

select value as employer, count(*) as cnt from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
CROSS APPLY 
OPENJSON ([employers])
where value is not null and value <> ''
group by value
order by cnt desc

select name, id, ci.value from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
OUTER APPLY 
OPENJSON ([children_id]) as ci
where id = 317561


select name, ci.value as child from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
OUTER APPLY 
OPENJSON ([children]) as ci
where name in ('Miloš Obrenović II')



-- ok, this doesn't work
;with CTE
AS
(
-- starting point
select name as n, data.id as id, ci.value as child_id from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
OUTER APPLY 
OPENJSON ([children_id]) as ci
where data.id = 297557
UNION ALL
select name as n, data.id as id, ci.value as child_id from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
OUTER APPLY 
OPENJSON ([children_id]) as ci
join CTE on CTE.child_id = data.id
)
select * from CTE



OPTION(MAXRECURSION 3)



select name , id from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as data
--CROSS APPLY 
--OPENJSON ([children_id]) as ci
where id in (317561
,434252
,75944852
,75944853
,75944849
,75944850
,75944851)
