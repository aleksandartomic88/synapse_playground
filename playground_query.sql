﻿select * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/container1/test.csv', format='csv') WITH (
  	col1 int, 
  	col2 INT
   ) as data

-- parquet example


-- example of accessing nested columns.
-- it is a bit strange that json_value has to be used...
select * from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/container1/*rep*.parquet', format='parquet') as data
where JSON_VALUE(population,'$[0]') > 2




   select * from sys.credentials

   IF EXISTS (SELECT * FROM sys.credentials WHERE name = N'https://polarisdata.blob.core.windows.net/csv')
BEGIN
DROP CREDENTIAL [https://atomicstorageaccount.blob.core.windows.net/container1]
END
GO

CREATE CREDENTIAL [https://atomicstorageaccount.blob.core.windows.net] 
WITH 
	IDENTITY = 'SHARED ACCESS SIGNATURE', 
	SECRET = 'sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2020-07-16T01:58:17Z&st=2020-02-29T18:58:17Z&spr=https&sig=znKxhP2meRYokxwMP5mMv2fERCm%2FdEgmtGsF0z1GlzY%3D'
GO

